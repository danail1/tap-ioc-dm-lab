package com.infinitelambda.di;

/**
 * Thrown to indicate problems with the container configuration.
 */
public class ContainerConfigurationException extends RuntimeException {

    public ContainerConfigurationException(String message) {
        super(message);
    }

    public ContainerConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

}
