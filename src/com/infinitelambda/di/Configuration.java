package com.infinitelambda.di;

import java.util.*;

public class Configuration {

    private Set<Bean> beans;

    public static Configuration create() {
        return new Configuration();
    }

    private Configuration() {
        beans = new HashSet<>();
    }

    public Configuration addBean(Class<?> beanClass, List<Class<?>> beanDependencies) {
        beans.add(Bean.create(beanClass, beanDependencies));
        return this;
    }

    Set<Bean> getBeans() {
        return beans;
    }
}
