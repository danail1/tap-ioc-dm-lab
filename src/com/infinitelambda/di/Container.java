package com.infinitelambda.di;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class Container {

    private final Map<Class<?>, Object> beans;

    public static Container from(Configuration configuration) {
        final Map<Class<?>, Object> beans = new HashMap<>();
        configuration.getBeans().stream()
                .map(bean -> getBeanInstance(bean, beans, configuration.getBeans()))
                .forEach(beanInstance -> beans.putIfAbsent(beanInstance.getClass(), beanInstance));
        return new Container(beans);
    }

    private static Object getBeanInstance(Bean bean, Map<Class<?>, Object> existingBeans, Set<Bean> configuredBeans) {
        return existingBeans.keySet().stream()
                .filter(existingBeanClass -> existingBeanClass.equals(bean.getBeanClass()))
                .findFirst()
                .map(existingBeans::get)
                .orElseGet(() -> createBeanInstance(bean, existingBeans, configuredBeans));
    }

    private static Object createBeanInstance(Bean bean, Map<Class<?>, Object> existingBeans, Set<Bean> configureBeans) {
        final Object[] dependencies = bean.getBeanDependencies().stream()
                .map(dependencyClass -> findDependencyBean(configureBeans, dependencyClass))
                .map(dependencyOptional -> dependencyOptional.orElseGet(() -> {
                    throw new ContainerConfigurationException("Could not process dependencies of " + bean.getBeanClass().getCanonicalName());
                }))
                .map(dependencyBean -> getBeanInstance(dependencyBean, existingBeans, configureBeans))
                .peek(dependencyInstance -> existingBeans.putIfAbsent(dependencyInstance.getClass(), dependencyInstance))
                .toArray();

        return invokeBeanConstructor(bean, dependencies);
    }

    private static Optional<Bean> findDependencyBean(Set<Bean> configuredBeans, Class<?> dependencyClass) {
        return configuredBeans.stream()
                .filter(bean -> dependencyClass.isAssignableFrom(bean.getBeanClass()))
                .findFirst();
    }

    private static Object invokeBeanConstructor(Bean bean, Object[] constructorParameters) {
        final Class<?> beanClass = bean.getBeanClass();
        try {
            final Constructor<?> beanConstructor = beanClass.getDeclaredConstructor(bean.getBeanDependencies().toArray(new Class<?>[0]));
            return beanConstructor.newInstance(constructorParameters);
        } catch (NoSuchMethodException e) {
            throw new ContainerConfigurationException("Unable to find a suitable constructor for class " + beanClass.getCanonicalName(), e);
        } catch (IllegalAccessException e) {
            throw new ContainerConfigurationException("Unable to access constructor for class " + beanClass.getCanonicalName(), e);
        } catch (InstantiationException | InvocationTargetException e) {
            throw new ContainerConfigurationException("Unable to instantiate class " + beanClass.getCanonicalName(), e);
        }
    }

    private Container(Map<Class<?>, Object> beans) {
        this.beans = beans;
    }

    public <T> T getBean(Class<T> beanClass) {
        //noinspection unchecked
        return (T) beans.get(beanClass);
    }
}
