package com.infinitelambda.di;

import java.util.List;
import java.util.Objects;

public class Bean {

    public static Bean create(Class<?> beanClass, List<Class<?>> beanDependencies) {
        return new Bean(beanClass, beanDependencies);
    }

    private final Class<?> beanClass;
    private final List<Class<?>> beanDependencies;

    private Bean(Class<?> beanClass, List<Class<?>> beanDependencies) {
        this.beanClass = beanClass;
        this.beanDependencies = beanDependencies;
    }

    Class<?> getBeanClass() {
        return beanClass;
    }

    List<Class<?>> getBeanDependencies() {
        return beanDependencies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bean bean = (Bean) o;
        return beanClass.equals(bean.beanClass) && beanDependencies.equals(bean.beanDependencies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(beanClass, beanDependencies);
    }
}
