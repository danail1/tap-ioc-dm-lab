package com.infinitelambda.greeter;

/**
 * Strategy context
 */
public class Greeter {

    private final MessageProvider messageProvider;

    public Greeter(MessageProvider messageProvider) {
        this.messageProvider = messageProvider;
    }

    public void greet() {
        final String message = messageProvider.provideMessage();
        System.out.println(message);
    }

}
