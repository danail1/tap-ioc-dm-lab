package com.infinitelambda.greeter;

/**
 * Represents the behaviour of a component that provides messages to a {@link Greeter}.
 *
 * Strategy interface
 */
public interface MessageProvider {

    String provideMessage();

}
