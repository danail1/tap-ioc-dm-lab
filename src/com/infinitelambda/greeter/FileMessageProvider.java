package com.infinitelambda.greeter;

public class FileMessageProvider implements MessageProvider {

    private final String file;

    public FileMessageProvider(String file) {
        this.file = file;
    }

    @Override
    public String provideMessage() {
        // TODO: read file and return content
        return null;
    }

}
