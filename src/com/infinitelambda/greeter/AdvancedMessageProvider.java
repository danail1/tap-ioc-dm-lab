package com.infinitelambda.greeter;

public class AdvancedMessageProvider implements MessageProvider{

    @Override
    public String provideMessage() {
        return "This is an advanced greeting";
    }

}
