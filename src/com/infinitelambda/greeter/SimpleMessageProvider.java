package com.infinitelambda.greeter;

/**
 * Specific strategy implementation
 */
public class SimpleMessageProvider implements MessageProvider {

    @Override
    public String provideMessage() {
        return "Hello, Talent Accelerator";
    }

}
