package com.infinitelambda;

import com.infinitelambda.di.Configuration;
import com.infinitelambda.di.Container;
import com.infinitelambda.greeter.Greeter;
import com.infinitelambda.greeter.MessageProvider;
import com.infinitelambda.greeter.SimpleMessageProvider;

import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        final Container container = Container.from(
                Configuration.create()
                        .addBean(SimpleMessageProvider.class, Collections.emptyList())
                        .addBean(Greeter.class, Collections.singletonList(MessageProvider.class))
        );

        final Greeter greeter = container.getBean(Greeter.class);
        greeter.greet();
    }
}
